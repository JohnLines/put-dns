% put-dns(1) put-dns 0.0.1
% John Lines
% January 2021

# NAME
put-dns -  Put a DNS record onto an external DNS provider

# SYNOPSIS
**put-dns** [*-v*] [*-n*] "DNS entry"

# DESCRIPTiON
**put-dns** uses the Provider defined in put-dns.conf to create a DNS entry.

## DNS entry
This is a DNS entry in RFC1034 format, this should be of the form
 owner type class RDATA
and passed as a single argument to put-dns.

# OPTIONS
**-v**
: Be verbose, say what DNS entry will be sent to which provider
**-n**
: No action - do not actually invoke the provider to add the entry

# EXAMPLES
put-dns \"mail.example.com IN A 10.9.8.7\"
: create a DNS Adress record for mail.example com, pointing to the IP addess 10.9.8.7

# COPYRIGHT
Copyright @ 2021 John Lines. License LGPLv3+: GNU Lesser GPL version 3 or later <http://www.gnu.org/licenses/lgpl-3.0.html>
This is free software: you are free to change and redistribute it. There is NO WARRENTY, to the extent permitted by law.
