put-dns aims to simplfy the process of adding DNS records for other projects, such as mail servers, which benefit from having MX, SPF, DKIM etc records set up for them.

It is targetted at software being installed in a fairly simple environment, as if the requirement extends to load balancers, sofware being split across multiple hosts and so on, then more detailed design and installation planning will be required.

It does not deal with Dynamic DNS. where a DNS entry, normally a simple A record, must be updated to point to a host on an IP address which changes from time to time, as this field is well covered.

There is one main command
 put-dns {dns-record-entry}

where {dns-record-entry} is a single entry in RFC 1034 format.

All the work is done by providers, which are simply scripts which do the real work of updating the external DNS provider used by the computer running put-dns with the DNS entry given in the argumnet.

The default 'none' provider simply appends the entry to a list of entries to be dealt with.

