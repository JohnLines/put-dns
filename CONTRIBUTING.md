Contributions, particularly of new DNS providers are very welcome. Please ensure that you do not give away your own API key, but, if needed, provide a dummy one under etc/private
